import { Component } from '@angular/core';
import {Solution} from "./solution.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Aqueous Outflow Model';
}
