export class Sinusotomy {
  public start: number;
  public end: number;

  constructor(start?: number, end?: number){
    this.start = start;
    this.end = end;
  }
}
