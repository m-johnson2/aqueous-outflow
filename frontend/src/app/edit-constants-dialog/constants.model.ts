export class Constants {
  public max_error: number;
  public n: number;
  public m: number;
  public etm: number;
  public rtm: number;
  public h0: number;
  public hs: number;
  public rcc: number;
  public pev: number;
  public qu: number;
}
