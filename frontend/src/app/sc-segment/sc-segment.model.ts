export class SCSegment {
  public seg: number;
  public height: number;

  constructor(seg: number, height: number) {
    this.seg = seg;
    this.height = height;
  }
}
