export class TMSegment {
  public hour: number;
  public resistance: number;

  constructor(hour: number, resistance: number) {
    this.hour = hour;
    this.resistance = resistance;
  }
}
