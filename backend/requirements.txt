-i https://pypi.org/simple
-e .
click==7.0
flask-cors==3.0.8
flask-marshmallow==0.10.1
flask==1.0.3
itsdangerous==1.1.0
jinja2==2.10.1
markupsafe==1.1.1
marshmallow==2.19.5
numpy==1.16.4
six==1.12.0
werkzeug==0.15.4
