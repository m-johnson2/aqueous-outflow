import setuptools

setuptools.setup(
    name='aqueous-outflow',
    version='1.0',
    author='Nicholas Farrar, BS/MS, Johnson Lab Northwestern University',
    author_email='NicholasFarrar2019@u.northwestern.edu',
    description='Fluid mechanical model of glaucoma and its surgical treatments',
    long_description_content_type='text/markdown',
    packages=setuptools.find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent'
    ]
)
